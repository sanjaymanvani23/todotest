package com.example.todotest.room

import com.example.todotest.room.data.ToDoData

class DatabaseHelperImpl(private val appDatabase: AppDatabase) : DatabaseHelper {

    override suspend fun getToDo(): List<ToDoData> = appDatabase.toDoDao().getAll()

    override suspend fun insertAll(users: List<ToDoData>) = appDatabase.toDoDao().insertAll(users)

    override suspend fun delete(todoList: ToDoData) = appDatabase.toDoDao().delete(todoList)

    override suspend fun update(todoList: ToDoData) = appDatabase.toDoDao().update(todoList)

}