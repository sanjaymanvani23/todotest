package com.example.todotest.room

import com.example.todotest.room.data.ToDoData

interface DatabaseHelper {

    suspend fun getToDo(): List<ToDoData>

    suspend fun insertAll(todoList: List<ToDoData>)

    suspend fun delete(todoList: ToDoData)

    suspend fun update(todoList: ToDoData)

}