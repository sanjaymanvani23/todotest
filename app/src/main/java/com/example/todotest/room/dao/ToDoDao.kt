package com.example.todotest.room.dao

import androidx.room.*
import com.example.todotest.room.data.ToDoData

@Dao
interface ToDoDao {

    @Query("SELECT * FROM ToDoData")
    suspend fun getAll(): List<ToDoData>

    @Insert
    suspend fun insertAll(users: List<ToDoData>)

    @Delete
    suspend fun delete(user: ToDoData)

    @Update
    suspend fun update(user: ToDoData)

}