package com.example.todotest.room

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.todotest.room.dao.ToDoDao
import com.example.todotest.room.data.ToDoData

@Database(entities = [ToDoData::class], version = 3)
abstract class AppDatabase : RoomDatabase() {

    abstract fun toDoDao(): ToDoDao

}