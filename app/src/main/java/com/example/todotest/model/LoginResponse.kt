package com.example.todotest.model

import com.google.gson.annotations.SerializedName

class LoginResponse {
    @SerializedName("token")
    val token: String? = null

    @SerializedName("error")
    var error: String? = null

}