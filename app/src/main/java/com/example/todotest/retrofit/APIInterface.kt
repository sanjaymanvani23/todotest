      package com.example.todotest.retrofit


import com.example.todotest.model.LoginResponse
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.*


interface APIInterface {

    @POST("login")
    suspend fun login(
            @Body body: JsonObject
    ): Response<LoginResponse?>

}