package com.example.todotest.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.evillageindia.myvillage.utils.callback.OnItemClick
import com.example.todotest.R
import com.example.todotest.room.data.ToDoData
import com.example.todotest.util.Constants

class CustomAdapter(private val dataSet: List<ToDoData>, itemClick: OnItemClick) :
        RecyclerView.Adapter<CustomAdapter.ViewHolder>() {


    private var itemClick: OnItemClick? = itemClick

    /**
     * Provide a reference to the type of views that you are using
     * (custom ViewHolder).
     */
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val textView: TextView
        val textViewDesc: TextView
        val textViewDate: TextView
        val imageviewDelete: ImageView
        val rlRoot: RelativeLayout


        init {
            // Define click listener for the ViewHolder's View.
            textView = view.findViewById(R.id.textViewTitle)
            textViewDesc = view.findViewById(R.id.textViewDesc)
            textViewDate = view.findViewById(R.id.textViewDate)
            imageviewDelete = view.findViewById(R.id.ivDelete)
            rlRoot = view.findViewById(R.id.rlRoot)
        }
    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        // Create a new view, which defines the UI of the list item
        val view = LayoutInflater.from(viewGroup.context)
                .inflate(R.layout.row_item, viewGroup, false)

        return ViewHolder(view)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {

        // Get element from your dataset at this position and replace the
        // contents of the view with that element
        viewHolder.textView.text = dataSet[position].title
        viewHolder.textViewDesc.text = dataSet[position].desc
        viewHolder.textViewDate.text = dataSet[position].date

        viewHolder.imageviewDelete.setOnClickListener {
            itemClick?.onItemClickCallBack(dataSet[position],Constants.DELETE,position)
        }

        viewHolder.rlRoot.setOnClickListener {
            itemClick?.onItemClickCallBack(dataSet[position],Constants.UPDATE,position)
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = dataSet.size

}
