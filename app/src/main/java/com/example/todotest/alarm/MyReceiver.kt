package com.example.todotest.alarm

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import com.example.todotest.util.Constants
import kotlinx.android.synthetic.main.fragment_add_todo.*

class MyReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        Log.i("Alarm","recieve "+intent.getStringExtra(Constants.TITLE))
        val intent1 = Intent(context, MyNewIntentService::class.java)
        intent1.putExtra(Constants.TITLE,intent.getStringExtra(Constants.TITLE))
        intent1.putExtra(Constants.DESC,intent.getStringExtra(Constants.DESC))
        intent1.putExtra(Constants.TIME,intent.getStringExtra(Constants.TIME))
        context.startService(intent1)
    }
}