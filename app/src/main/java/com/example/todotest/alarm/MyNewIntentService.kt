package com.example.todotest.alarm

import android.app.*
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.example.todotest.MainActivity
import com.example.todotest.R
import com.example.todotest.util.Constants

class MyNewIntentService : IntentService("MyNewIntentService") {
    override fun onHandleIntent(intent: Intent?) {

        intent?.getStringExtra(Constants.TITLE)?.let { sendNotification(it,
            intent.getStringExtra(Constants.DESC)!!+"\n"+ intent.getStringExtra(Constants.TIME)!!
        ) }
    }

    companion object {
        private const val NOTIFICATION_ID = 3
    }

    private fun sendNotification(title: String, messageBody: String) {
        val intent = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)

        val pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent, PendingIntent.FLAG_ONE_SHOT)

        val channelId = getString(R.string.default_notification_channel_id)
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder = NotificationCompat.Builder(this, channelId)
            .setSmallIcon(R.mipmap.ic_launcher)
            //.setContentTitle(getString(R.string.fcm_message))
            .setContentTitle(title)
            .setContentText(messageBody)
            .setDefaults(Notification.DEFAULT_ALL)
            .setAutoCancel(true)
            .setPriority(NotificationCompat.PRIORITY_MAX)
            .setSound(defaultSoundUri)
            .setContentIntent(pendingIntent)

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            (NotificationManager.IMPORTANCE_HIGH)
        }
        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(channelId, "Channel human readable title", NotificationManager.IMPORTANCE_HIGH)
            notificationBuilder.priority = NotificationCompat.PRIORITY_HIGH
            notificationManager.createNotificationChannel(channel)

        }

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build())
    }

}