package com.example.todotest.util

import android.app.Activity
import android.view.View
import androidx.appcompat.app.AlertDialog
import com.example.todotest.R
import java.util.regex.Pattern


class Constants {
    companion object {
        private const val HOST_URL = "https://reqres.in/"
        var BASE_URL = HOST_URL + "api/"

        lateinit var dialog: AlertDialog

        val TOKEN = "token"
        val DELETE = "delete"
        val DATA = "data"
        val ADDED = "added"
        val UPDATE = "update"
        val TITLE = "title"
        val DESC = "desc"
        val TIME = "time"

        val EMAIL = "email"
        val PASSWORD = "password"


        val EMAIL_ADDRESS_PATTERN: Pattern = Pattern.compile(
            "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
                    "\\@" +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                    "(" +
                    "\\." +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
                    ")+"
        )

         fun checkEmail(email: String): Boolean {
            return EMAIL_ADDRESS_PATTERN.matcher(email).matches()
        }


        fun callProgressDailog(activity: Activity) {
            val builder: AlertDialog.Builder = AlertDialog.Builder(activity)
            val customLayout: View = activity.layoutInflater.inflate(R.layout.progress, null)
            builder.setView(customLayout)
            dialog = builder.create()
            /* dialog.setCanceledOnTouchOutside(false)
             dialog.setCancelable(false)*/
            dialog.show()
        }

        fun hideProgressDialog() {
            if(this::dialog.isInitialized) {
                dialog.dismiss()
            }
        }


    }

}