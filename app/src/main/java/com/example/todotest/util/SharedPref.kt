package com.example.todotest.util

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson
import java.lang.reflect.Type

object SharedPref {
    private var shr: SharedPreferences? = null
    private var editor: SharedPreferences.Editor? = null
    var prefName = "ToDoList"
    operator fun contains(key: String?): Boolean {
        return shr!!.contains(key)
    }

    fun initSharedPref(context: Context) {
        shr =
            context.getSharedPreferences(prefName, Context.MODE_PRIVATE)
    }

    fun saveString(key: String, value: String?) {
        editor = shr!!.edit()
        editor?.putString(key, value)?.apply()
    }

    fun getString(key: String): String? {
        return shr!!.getString(key, "")
    }

    fun saveInt(key: String?, value: Int) {
        try {
            editor = shr!!.edit()
            editor?.putInt(key, value)?.apply()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun saveBoolean(key: String?, value: Boolean) {
        try {
            editor = shr!!.edit()
            editor?.putBoolean(key, value)?.apply()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


    fun saveObject(
        `object`: Any?,
        context: Context,
        key: String
    ) {
        shr =
            context.getSharedPreferences(prefName, Context.MODE_PRIVATE)
        editor = shr?.edit()
        val gson = Gson()
        val json = gson.toJson(`object`)
        editor?.putString(key, json)
        editor?.apply()

    }

    fun getObject(
        context: Context,
        key: String?,
        classname: Type?
    ): Any? {
        shr =
            context.getSharedPreferences(prefName, Context.MODE_PRIVATE)
        val gson = Gson()
        val json = shr?.getString(key, "")
        return gson.fromJson(json, classname)
    }

    fun cleardata() {
        try {
            editor = shr!!.edit()
            editor?.clear()?.apply()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}