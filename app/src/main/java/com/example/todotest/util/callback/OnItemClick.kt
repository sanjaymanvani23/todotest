package com.evillageindia.myvillage.utils.callback

interface OnItemClick {
    fun onItemClickCallBack(data: Any, screen: String, pos: Int)
}
