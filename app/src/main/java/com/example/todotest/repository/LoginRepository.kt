package com.example.todotest.repository

import androidx.lifecycle.MutableLiveData
import com.example.todotest.MyApplication
import com.example.todotest.model.LoginResponse
import com.example.todotest.util.Constants
import com.google.gson.Gson
import com.google.gson.JsonObject
import kotlinx.coroutines.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginRepository {
    val TAG = LoginRepository::class.java.simpleName
    private var repository: LoginRepository? = null
    var job: Job? = null
    val instance: LoginRepository?
        get() {
            if (repository == null) {
                repository = LoginRepository()
            }
            return repository
        }


     fun login(
        email: String?,
        password: String?
    ): MutableLiveData<LoginResponse?> {
        val mutableLiveData: MutableLiveData<LoginResponse?> =
            MutableLiveData<LoginResponse?>()

         val jsonObject = JsonObject()

        jsonObject.addProperty(Constants.EMAIL,  email)
        jsonObject.addProperty(Constants.PASSWORD, password)

        job = CoroutineScope(Dispatchers.IO ).launch {
            val response = MyApplication.apiInterface?.login(jsonObject)
            withContext(Dispatchers.Main) {
                if (response?.code() == 200) {
                    mutableLiveData.setValue(response.body())
                } else {
                    val responseData: LoginResponse = Gson().fromJson(
                        response?.errorBody()!!.charStream(),
                        LoginResponse::class.java
                    )
                    mutableLiveData.setValue(responseData)

                }
            }
        }

       /* val call: Call<LoginResponse?>? = MyApplication.apiInterface?.login("",jsonObject)
        call?.enqueue(object : Callback<LoginResponse?> {
            override fun onResponse(
                call: Call<LoginResponse?>,
                response: Response<LoginResponse?>
            ) {
                if (response.code() == 200) {
                    mutableLiveData.setValue(response.body())
                } else {
                    val responseData: LoginResponse = Gson().fromJson(
                        response.errorBody()!!.charStream(),
                        LoginResponse::class.java
                    )
                    mutableLiveData.setValue(responseData)

                }
            }

            override fun onFailure(call: Call<LoginResponse?>, t: Throwable) {
                mutableLiveData.value = null

            }
        })*/
        return mutableLiveData
    }





}