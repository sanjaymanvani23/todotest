package com.example.todotest.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.todotest.room.DatabaseHelper
import com.example.todotest.room.data.ToDoData
import com.example.todotest.util.Constants
import kotlinx.coroutines.launch

class RoomDBViewModel(private val dbHelper: DatabaseHelper) :
    ViewModel() {

    private val list = MutableLiveData<List<ToDoData>>()
    private val delete = MutableLiveData<String>()
    private val add = MutableLiveData<String>()

    /*init {
        fetchToDo()
    }*/

     fun fetchToDo() {
        viewModelScope.launch {
            try {
                val toDoFromDb = dbHelper.getToDo()

                // here you have your usersFromDb

                list.setValue(toDoFromDb)
            } catch (e: Exception) {
                // handler error
            }
        }
    }

    fun addData(todoList: List<ToDoData>){
        viewModelScope.launch {
            try {
                val toDoFromDb = dbHelper.insertAll(todoList)
                add.setValue(Constants.ADDED)
                // here you have your usersFromDb

            } catch (e: Exception) {
                // handler error
            }
        }
    }

    fun updateData(todoList: ToDoData){
        viewModelScope.launch {
            try {
                val toDoFromDb = dbHelper.update(todoList)
                add.setValue(Constants.ADDED)
                // here you have your usersFromDb

            } catch (e: Exception) {
                // handler error
            }
        }
    }

    fun deleteTodo(todoList: ToDoData){
        viewModelScope.launch {
            try {
                val toDoFromDb = dbHelper.delete(todoList)

                delete.setValue(Constants.DELETE)

            } catch (e: Exception) {
                // handler error
            }
        }
    }

    fun getTodo(): LiveData<List<ToDoData>> {
        return list
    }

    fun delete(): LiveData<String> {
        return delete
    }

    fun add(): LiveData<String> {
        return add
    }



}