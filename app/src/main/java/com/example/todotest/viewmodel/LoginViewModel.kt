package com.example.todotest.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.todotest.model.LoginResponse
import com.example.todotest.repository.LoginRepository
import com.example.todotest.util.Constants

class LoginViewModel : ViewModel() {
    private var mutableLiveData: MutableLiveData<LoginResponse?>? = null

    fun init(email: String?, password: String?) {
             var repository = LoginRepository().instance
             mutableLiveData = repository?.login(email, password)!!
         }

    fun requestLogin(): LiveData<LoginResponse?>? {
        return mutableLiveData
    }


}