package com.example.todotest

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Application
import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.util.Log
import com.example.todotest.retrofit.APIClient
import com.example.todotest.retrofit.APIInterface
import com.example.todotest.util.SharedPref


class MyApplication : Application() {

    companion object {
        var context : Context?=null
        var apiInterface: APIInterface? = null
    }

    override fun onCreate() {
        super.onCreate()
        context = applicationContext
        SharedPref.initSharedPref(this)
        apiInterface = APIClient.client!!.create(APIInterface::class.java)


    }

}