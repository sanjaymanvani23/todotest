package com.example.todotest.ui.main

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.example.todotest.R
import com.example.todotest.util.Constants
import com.example.todotest.util.SharedPref
import com.example.todotest.viewmodel.LoginViewModel
import kotlinx.android.synthetic.main.fragment_login.*


class LoginFragment : Fragment() {

    companion object {
        fun newInstance() = LoginFragment()
    }

    private lateinit var viewModel: LoginViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        if(SharedPref.getString(Constants.TOKEN)?.isNotEmpty()!!){
            findNavController().navigate(R.id.action_loginFragment_to_homeFragment)

        }
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(LoginViewModel::class.java)

        login.setOnClickListener {
            when {
                edEmail.text.toString().isEmpty() -> {
                    Toast.makeText(requireContext(),getString(R.string.enter_email),Toast.LENGTH_SHORT).show()
                }
                !Constants.checkEmail(edEmail.text.toString()) -> {
                    Toast.makeText(requireContext(),getString(R.string.email_valid),Toast.LENGTH_SHORT).show()
                }
                edPassword.text.toString().isEmpty() -> {
                    Toast.makeText(requireContext(),getString(R.string.enter_password),Toast.LENGTH_SHORT).show()
                }
                else -> {
                    callApi()
                }
            }
        }

    }

    private fun callApi(){
        Constants.callProgressDailog(requireActivity())
        viewModel.init(edEmail.text.toString(), edPassword.text.toString())
        viewModel.requestLogin()?.observe(viewLifecycleOwner, {
            Constants.hideProgressDialog()
            if (it?.token?.isNotEmpty() == true) {
                    SharedPref.saveString(Constants.TOKEN,it?.token.toString())
                requireActivity().findNavController(R.id.my_nav_host_fragment).navigate(R.id.homeFragment, null)
            }else{
                Toast.makeText(requireContext(),it?.error.toString(),Toast.LENGTH_SHORT).show()
            }
        })
    }

}