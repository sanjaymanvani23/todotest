package com.example.todotest.ui.main

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.evillageindia.myvillage.utils.callback.OnItemClick
import com.example.todotest.R
import com.example.todotest.adapter.CustomAdapter
import com.example.todotest.room.DatabaseBuilder
import com.example.todotest.room.DatabaseHelperImpl
import com.example.todotest.room.data.ToDoData
import com.example.todotest.util.Constants
import com.example.todotest.viewmodel.RoomDBViewModel
import com.example.todotest.viewmodel.ViewModelFactory
import kotlinx.android.synthetic.main.fragment_home.*



class HomeFragment : Fragment(), OnItemClick {

    companion object {
        fun newInstance() = HomeFragment()
    }

    private lateinit var viewModel: RoomDBViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        fab.setOnClickListener {
            findNavController().navigate(R.id.action_homeFragment_to_addTodoFragment)
        }

        val dbHelper = DatabaseHelperImpl(DatabaseBuilder.getInstance(requireContext()))

        viewModel = ViewModelProvider(
            this@HomeFragment,
            ViewModelFactory(
                dbHelper
            )
        ).get(RoomDBViewModel::class.java)

     //   getList()


    }

    override fun onResume() {
        super.onResume()
        getList()
    }

    private fun getList(){

        viewModel.fetchToDo()
        viewModel.getTodo().observe(viewLifecycleOwner,{
            val mdAdapter = CustomAdapter(
                it,
                this@HomeFragment
            )
            val manager = LinearLayoutManager(activity)


            rvList?.apply {
                layoutManager = manager
                itemAnimator = DefaultItemAnimator()
                adapter = mdAdapter
            }
        })

    }

    override fun onItemClickCallBack(data: Any, screen: String, pos: Int) {
        data as ToDoData
        when(screen){
                Constants.DELETE->{
                    deleteConfirmation(data)
                }
            Constants.UPDATE->{
                val bundle = Bundle()
                bundle.putParcelable(Constants.DATA,data)
                findNavController().navigate(R.id.action_homeFragment_to_addTodoFragment,bundle)
            }
            }
    }

    private fun delete(data : ToDoData){
        viewModel.deleteTodo(data)

        viewModel.delete().observe(viewLifecycleOwner,{
            if(it.equals(Constants.DELETE)){
                getList()
            }
        })
    }


    private fun deleteConfirmation(data : ToDoData){
        val builder = AlertDialog.Builder(requireActivity())
        //set title for alert dialog
        builder.setTitle(R.string.delete)
        //set message for alert dialog
        builder.setMessage(R.string.delete_msg)


        //performing positive action
        builder.setPositiveButton("Yes"){dialogInterface, which ->
            delete(data)
        }
        //performing cancel action
        builder.setNeutralButton("No"){dialogInterface , which ->

        }

        // Create the AlertDialog
        val alertDialog: AlertDialog = builder.create()
        // Set other dialog properties
        alertDialog.setCancelable(false)
        alertDialog.show()
    }

}