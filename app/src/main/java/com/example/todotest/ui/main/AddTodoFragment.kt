package com.example.todotest.ui.main

import android.app.AlarmManager
import android.app.DatePickerDialog
import android.app.PendingIntent
import android.app.TimePickerDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TimePicker
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.example.todotest.R
import com.example.todotest.alarm.MyReceiver
import com.example.todotest.room.DatabaseBuilder
import com.example.todotest.room.DatabaseHelperImpl
import com.example.todotest.room.data.ToDoData
import com.example.todotest.util.Constants
import com.example.todotest.viewmodel.RoomDBViewModel
import com.example.todotest.viewmodel.ViewModelFactory
import kotlinx.android.synthetic.main.fragment_add_todo.*
import kotlinx.android.synthetic.main.fragment_login.*
import java.util.*
import kotlin.collections.ArrayList


class AddTodoFragment : Fragment(), View.OnClickListener {

    companion object {
        fun newInstance() = AddTodoFragment()
    }

    var hour = 0
    var minute = 0
    var year = 0
    var month = 0
    var day = 0

    lateinit var newDate : Calendar

    private lateinit var viewModel: RoomDBViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.fragment_add_todo, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        if(arguments!=null){
            val data : ToDoData? = arguments?.getParcelable(Constants.DATA)
            edTitle.setText(data?.title)
            edDesc.setText(data?.desc)

            add.text = "Update"

            if(data?.time?.contains(":")!!) {
                var time = data?.time?.split(":")
                hour = time[0].trim().toInt()
                minute = time[1].trim().toInt()
                edTime.setText(data?.time)
            }

            if(data?.date?.contains("-")!!) {
                var date = data?.date?.split("-")
                day = date[0].trim().toInt()
                month = date[1].trim().toInt()-1
                year = date[2].trim().toInt()

                edDate.setText(data?.date)

                newDate = Calendar.getInstance()
                newDate.set(year, month, day)
            }

            when(data?.type){
                "Weekly"->{
                    rbWeekly.isChecked=true
                }else->{
                    rbDaily.isChecked=true
                }
            }
        }

        val dbHelper = DatabaseHelperImpl(DatabaseBuilder.getInstance(requireContext()))

        viewModel = ViewModelProvider(
            this@AddTodoFragment,
            ViewModelFactory(
                dbHelper
            )
        ).get(RoomDBViewModel::class.java)

        viewModel.add().observe(viewLifecycleOwner,{
            Log.i("Alarm ","call")
            if(it.equals(Constants.ADDED)){
                Log.i("Alarm ","back")
                setAlarm()
                requireActivity().findNavController(R.id.my_nav_host_fragment).popBackStack()
            }
        })

        add.setOnClickListener(this)
        edDate.setOnClickListener(this)
        edTime.setOnClickListener(this)

    }

    private fun setAlarm(){

        val alarmIntent = Intent(requireActivity(), MyReceiver::class.java)
        alarmIntent.putExtra(Constants.TITLE,edTitle.text.toString())
        alarmIntent.putExtra(Constants.DESC,edDesc.text.toString())
        alarmIntent.putExtra(Constants.TIME,edDate.text.toString()+" "+edTime.text.toString())
        val pendingIntent = PendingIntent.getBroadcast(requireActivity(), 0, alarmIntent, PendingIntent.FLAG_CANCEL_CURRENT)

        val manager = requireActivity().getSystemService(Context.ALARM_SERVICE) as AlarmManager?

      //  val calendar = Calendar.getInstance()
       // calendar.timeInMillis = System.currentTimeMillis()
        newDate[Calendar.HOUR_OF_DAY] = hour
        newDate[Calendar.MINUTE] = minute

        var day = 1
        if(rbWeekly.isChecked){
            day=7
        }

        manager!!.setRepeating(
            AlarmManager.RTC_WAKEUP, newDate.timeInMillis,
            AlarmManager.INTERVAL_DAY * day, pendingIntent
        )
    }

    override fun onClick(p0: View?) {
        when(p0?.id){
            R.id.add->{
                addData()
            }
            R.id.edDate->{
                if(year==0) {
                    val c = Calendar.getInstance()
                    year = c.get(Calendar.YEAR)
                    month = c.get(Calendar.MONTH)
                    day = c.get(Calendar.DAY_OF_MONTH)
                }
                selectDate()
            }
            R.id.edTime->{
                if(hour==0) {
                    val mcurrentTime = Calendar.getInstance()
                    hour = mcurrentTime.get(Calendar.HOUR_OF_DAY)
                    minute = mcurrentTime.get(Calendar.MINUTE)
                }
                selectTime()
            }
        }
    }

    private fun selectDate(){

        val dpd = DatePickerDialog(requireActivity(), DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->

             newDate = Calendar.getInstance()
            newDate.set(year, monthOfYear, dayOfMonth)

          val tempMonth =  monthOfYear+1
            // Display Selected date in textbox
            edDate.setText("$dayOfMonth-$tempMonth-$year")

        }, year, month, day)

        dpd.show()
    }

    private fun selectTime(){
        val mTimePicker: TimePickerDialog

        mTimePicker = TimePickerDialog(requireActivity(), object : TimePickerDialog.OnTimeSetListener {
            override fun onTimeSet(view: TimePicker?, hourOfDay: Int, minute: Int) {
                edTime.setText(String.format("%d : %d", hourOfDay, minute))
            }
        }, hour, minute, false)

        mTimePicker.show()
    }

    private fun addData(){
        when {
            edTitle.text.toString().isEmpty() -> {
                Toast.makeText(requireContext(),getString(R.string.enter_title), Toast.LENGTH_SHORT).show()
            }
            edDesc.text.toString().isEmpty() -> {
                Toast.makeText(requireContext(),getString(R.string.enter_desc), Toast.LENGTH_SHORT).show()
            }
            edTime.text.toString().isEmpty() -> {
                Toast.makeText(requireContext(),getString(R.string.enter_time), Toast.LENGTH_SHORT).show()
            }
            edDate.text.toString().isEmpty() -> {
                Toast.makeText(requireContext(),getString(R.string.enter_date), Toast.LENGTH_SHORT).show()
            }
            (!rbDaily.isChecked && !rbWeekly.isChecked)->{
                Toast.makeText(requireContext(),getString(R.string.enter_type), Toast.LENGTH_SHORT).show()
            }
            else -> {
                var list = ArrayList<ToDoData>()
                var type = if(rbDaily.isChecked)  "Daily" else "Weekly"
                var id : Int? = null
                if(add.text.equals("Update")){
                    val data : ToDoData? = arguments?.getParcelable(Constants.DATA)
                   id = data?.id
                }
                 var toDoData = ToDoData(id,edTitle.text.toString(),edDesc.text.toString(),
                     edTime.text.toString(),edDate.text.toString(),type)

                list.add(toDoData)

                if(add.text.equals("Update")){
                    Log.i("primary u",toDoData.id.toString())
                    viewModel.updateData(toDoData)
                }else{
                    viewModel.addData(list)
                }

            }
        }
    }

}