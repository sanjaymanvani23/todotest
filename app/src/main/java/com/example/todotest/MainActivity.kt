package com.example.todotest

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import com.example.todotest.ui.main.HomeFragment
import com.example.todotest.ui.main.LoginFragment
import com.example.todotest.util.Constants
import com.example.todotest.util.SharedPref

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)

        /*if(SharedPref.getString(Constants.TOKEN)?.isNotEmpty()!!){
            findNavController(R.id.my_nav_host_fragment).navigate(R.id.homeFragment, null)
        }else{
            findNavController(R.id.my_nav_host_fragment).navigate(R.id.loginFragment, null)
        }*/

    }

    override fun onBackPressed() {
        super.onBackPressed()

        val fragments = supportFragmentManager.fragments

        for (fragment in fragments) {

            if (fragment.javaClass.name == NavHostFragment().javaClass.name) {
                val subfragments = fragment.childFragmentManager.fragments

                for (fragment1: Fragment in subfragments) {
                    when (fragment1.javaClass.name) {
                        HomeFragment().javaClass.name -> {
                            finish()
                        }
                    }
                }
            }
        }
    }
}